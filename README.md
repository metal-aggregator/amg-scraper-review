Sample input:
```json
{
  "reviewUrl": "https://www.angrymetalguy.com/goden-beyond-darkness-review/"
}
```

Sample output:
```json
{
  "band":"Goden",
  "album":"Beyond Darkness",
  "url":"http://www.angrymetalguy.com/goden-beyond-darkness-review/",
  "tags":[
    "2020",
    "3.5",
    "Abbath",
    "American Metal",
    "Beyond Darkness",
    "Black Sabbath",
    "Crowbar",
    "Death Metal",
    "death/doom metal",
    "Dio",
    "Doom Metal",
    "Goden",
    "Gorguts",
    "Hanzel und Gretyl",
    "Heaven and Hell",
    "Immortal",
    "May20",
    "Morbus Chron",
    "Necros Christos",
    "Paysage d'Hiver",
    "Review",
    "Reviews",
    "Sludge Metal",
    "Sunless",
    "Svart Records",
    "Sweven",
    "Winter"
  ]
}
```

Environment variables:
TBD