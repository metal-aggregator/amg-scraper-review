package com.ryan.amg.scraper.review.service.amg

import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements
import spock.lang.Specification

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class TagParserSpec extends Specification {

    TagParser tagParser = new TagParser()

    def "Invoking parseTags correctly pulls the tags out of the input Document"() {

        given:
            Document inputDocument = Mock()

            Element mockElement1_1 = Mock()
            1 * mockElement1_1.text() >> 'tag1'

            Element mockElement1_2 = Mock()
            1 * mockElement1_2.text() >> 'tag2'

            1 * inputDocument.getElementsByAttributeValue('rel', 'tag') >> new Elements([mockElement1_1, mockElement1_2])

            List<String> expectedTags = ['tag1', 'tag2']

        when:
            List<String> actualTags = tagParser.parseTags(inputDocument)

        then:
            assertReflectionEquals(expectedTags, actualTags)

    }

}
