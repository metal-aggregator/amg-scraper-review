package com.ryan.amg.scraper.review.service

import com.ryan.amg.scraper.review.domain.AlbumReview
import com.ryan.amg.scraper.review.domain.AlbumReviewBuilder
import com.ryan.amg.scraper.review.service.amg.ReviewPageParsingService
import org.unitils.reflectionassert.ReflectionComparatorMode
import spock.lang.Specification

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class ReviewPageScrapingServiceSpec extends Specification {

    ReviewPageParsingService mockReviewPageParsingService = Mock()

    ReviewPageScrapingService reviewPageScrapingService = new ReviewPageScrapingService(mockReviewPageParsingService)

    def "Invoking reviewAndParseAmg correctly calls its dependencies and returns the expected list of URLs"() {

        given:
            String inputUrl = 'http://www.url1.com'

            AlbumReview mockedAlbumReviews = new AlbumReviewBuilder().build()
            AlbumReview expectedAlbumReviews = new AlbumReviewBuilder().build()

            1 * mockReviewPageParsingService.parseReview(inputUrl) >> mockedAlbumReviews

        when:
            AlbumReview actualAlbumReview = reviewPageScrapingService.retrieveAndParseAmg(inputUrl)

        then:
            assertReflectionEquals(expectedAlbumReviews, actualAlbumReview, ReflectionComparatorMode.LENIENT_ORDER)

    }

}
