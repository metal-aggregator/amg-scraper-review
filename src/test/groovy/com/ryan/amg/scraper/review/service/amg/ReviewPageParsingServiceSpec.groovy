package com.ryan.amg.scraper.review.service.amg

import com.ryan.amg.scraper.review.delegate.WebsiteDelegate
import com.ryan.amg.scraper.review.domain.AlbumReview
import com.ryan.amg.scraper.review.domain.AlbumReviewBuilder
import com.ryan.amg.scraper.review.domain.mapper.AlbumReviewMapper
import org.jsoup.nodes.Document
import spock.lang.Specification

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class ReviewPageParsingServiceSpec extends Specification {

    WebsiteDelegate mockWebsiteDelegate = Mock()
    MetadataParser mockMetadataParser = Mock()
    TagParser mockTagParser = Mock()
    AlbumReviewMapper mockAlbumReviewMapper = Mock()

    ReviewPageParsingService reviewPageParsingService = new ReviewPageParsingService(mockWebsiteDelegate, mockMetadataParser, mockTagParser, mockAlbumReviewMapper)

    def "Invoking parseReviews correctly interacts with the WebsiteDelegate, parses the Document, and returns a list of AlbumReviews"() {

        given:
            String inputUrl = 'http://url1.com'

            Document mockedDocument = Mock()

            Map<String, String> mockedMetadata = new HashMap<>(['property1_1': 'content1_1', 'property1_2': 'content1_2'])
            Map<String, String> expectedMetadata = new HashMap<>(['property1_1': 'content1_1', 'property1_2': 'content1_2'])

            List<String> mockedTags = ['tag1', 'tag2']
            List<String> expectedTags = ['tag1', 'tag2']

            AlbumReview expectedAlbumReview = new AlbumReviewBuilder().build()
            AlbumReview mockedAlbumReview = new AlbumReviewBuilder().build()

            1 * mockWebsiteDelegate.retrieveWebsite(inputUrl) >> mockedDocument
            1 * mockMetadataParser.parseMetadata(_ as Document) >> mockedMetadata
            1 * mockTagParser.parseTags(_ as Document) >> mockedTags
            1 * mockAlbumReviewMapper.buildAlbumReview(_ as Map<String, String>, _ as List<String>) >> {args ->
                assertReflectionEquals(expectedMetadata, (Map<String, String>) args.get(0))
                assertReflectionEquals(expectedTags, args.get(1))
                return mockedAlbumReview
            }

        when:
            AlbumReview actualAlbumReview = reviewPageParsingService.parseReview(inputUrl)

        then:
            assertReflectionEquals(expectedAlbumReview, actualAlbumReview)

    }

}
