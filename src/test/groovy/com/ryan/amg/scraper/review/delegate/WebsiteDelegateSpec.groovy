package com.ryan.amg.scraper.review.delegate

import com.ryan.amg.scraper.review.delegate.remote.DocumentFetcher
import org.jsoup.nodes.Document
import spock.lang.Specification

class WebsiteDelegateSpec extends Specification {

    DocumentFetcher mockDocumentFetcher = Mock()

    WebsiteDelegate websiteDelegate

    def setup() {
        websiteDelegate = new WebsiteDelegate(mockDocumentFetcher)
    }

    def "Invoking retrieveWebsite invokes the DocumentFetcher to return a Document representation of the AMG URL"() {

        given:
            String inputUrl = 'https://www.angrymetalguy.com'

            Document mockedDocument = Mock()
            1 * mockDocumentFetcher.fetchDocument(inputUrl) >> mockedDocument

        when:
            Document actualDocument = websiteDelegate.retrieveWebsite(inputUrl)

        then:
            actualDocument

    }

}
