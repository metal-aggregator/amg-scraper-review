package com.ryan.amg.scraper.review.domain.mapper

import com.ryan.amg.scraper.review.domain.AlbumReview
import com.ryan.amg.scraper.review.domain.BandAndAlbumName
import org.unitils.reflectionassert.ReflectionComparatorMode
import spock.lang.Specification

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class AlbumReviewMapperSpec extends Specification {

    BandAndAlbumNameMapper mockBandAndAlbumNameMapper = Mock()

    AlbumReviewMapper rawTagMapper = new AlbumReviewMapper(mockBandAndAlbumNameMapper)

    def "Invoking parseRawTagsToAlbumReview with a well-formed map interacts with the BandAndAlbumNameMapper and returns a well-formed AlbumReview"() {

        given:
            String inputTitle = 'BandName - AlbumName Review | Angry Metal Guy'

            String expectedBandName = 'BandName'
            String expectedAlbumName = 'AlbumName'
            String expectedUrl = 'http://someUrl.com'
            String expectedImageUrl = 'http://someImageUrl.com'
            String tag1 = 'tag1'
            String tag2 = 'tag2'

            List<String> inputTags = [tag1, tag2]

            BandAndAlbumName mockBandAndAlbumName = new BandAndAlbumName(expectedBandName, expectedAlbumName)

            Map<String, String> inputRawTags = new HashMap<>()
            inputRawTags.put('og:title', inputTitle)
            inputRawTags.put('og:url', expectedUrl)
            inputRawTags.put('og:image:secure_url', expectedImageUrl)

            AlbumReview expectedAlbumReview = new AlbumReview(expectedBandName, expectedAlbumName, expectedUrl, [tag1, tag2], expectedImageUrl)

            1 * mockBandAndAlbumNameMapper.parseBandAndAlbumName(inputTitle) >> mockBandAndAlbumName

        when:
            AlbumReview actualAlbumReview = rawTagMapper.buildAlbumReview(inputRawTags, inputTags)

        then:
            assertReflectionEquals(expectedAlbumReview, actualAlbumReview, ReflectionComparatorMode.LENIENT_ORDER)

    }

    def "Invoking parseRawTagsToAlbumReview with a non-matching map interacts with the BandAndAlbumNameMapper and returns an empty AlbumReview"() {

        given:
            AlbumReview expectedAlbumReview = new AlbumReview(band: 'UNKNOWN', album: 'UNKNOWN')

            1 * mockBandAndAlbumNameMapper.parseBandAndAlbumName(null) >> new BandAndAlbumName('UNKNOWN', 'UNKNOWN')

        when:
            AlbumReview actualAlbumReview = rawTagMapper.buildAlbumReview(new HashMap<>(), Collections.emptyList())

        then:
            assertReflectionEquals(expectedAlbumReview, actualAlbumReview)

    }

}
