package com.ryan.amg.scraper.review.domain

class AlbumReviewBuilder {
    String band = 'Nile'
    String album = 'The Black Seeds of Vengeance'
    String url = 'https://www.nile.com'
    List<String> tags = ['5', 'Death Metal', 'American Metal']
    String imageUrl = 'https://www.nile.com/logo.jpg'

    AlbumReview build() {
        return new AlbumReview(
            band: band,
            album: album,
            url: url,
            tags: tags,
            imageUrl: imageUrl
        )
    }
}
