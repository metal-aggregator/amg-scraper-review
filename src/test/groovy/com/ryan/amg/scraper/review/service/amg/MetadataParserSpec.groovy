package com.ryan.amg.scraper.review.service.amg

import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements
import spock.lang.Specification

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class MetadataParserSpec extends Specification {

    MetadataParser metadataParser = new MetadataParser()

    def "Invoking parseMetadata correctly parses the base metadata out of the review Document"() {

        given:
            Document inputDocument = Mock()

            Element mockElement1_1 = Mock()
            1 * mockElement1_1.attr('property') >> 'property1_1'
            1 * mockElement1_1.attr('content') >> 'content1_1'

            Element mockElement1_2 = Mock()
            1 * mockElement1_2.attr('property') >> 'property1_2'
            1 * mockElement1_2.attr('content') >> 'content1_2'

            1 * inputDocument.getElementsByTag('meta') >> new Elements([mockElement1_1, mockElement1_2])

            Map<String, String> expectedParsedMetadata = new HashMap<>(['property1_1': 'content1_1', 'property1_2': 'content1_2'])

        when:
            Map<String, String> actualParsedMetadata = metadataParser.parseMetadata(inputDocument)

        then:
            assertReflectionEquals(expectedParsedMetadata, actualParsedMetadata)

    }

}
