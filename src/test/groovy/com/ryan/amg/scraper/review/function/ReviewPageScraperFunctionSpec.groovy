package com.ryan.amg.scraper.review.function

import com.ryan.amg.scraper.review.domain.AlbumReview
import com.ryan.amg.scraper.review.domain.AlbumReviewBuilder
import com.ryan.amg.scraper.review.dto.ScrapeReviewPageDTO
import com.ryan.amg.scraper.review.service.ReviewPageScrapingService
import spock.lang.Specification

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class ReviewPageScraperFunctionSpec extends Specification {

    ReviewPageScrapingService mockReviewPageScrapingService = Mock()

    ReviewPageScraperFunction reviewPageScraperFunction = new ReviewPageScraperFunction(mockReviewPageScrapingService)

    def "Invoking apply calls its service layer correctly and returns the expected AlbumReview"() {

        given:
            AlbumReview mockedAlbumReview = new AlbumReviewBuilder().build()
            AlbumReview expectedAlbumReview = new AlbumReviewBuilder().build()

            String inputUrl = 'http://testurl.com'

            1 * mockReviewPageScrapingService.retrieveAndParseAmg(inputUrl) >> mockedAlbumReview

        when:
            AlbumReview actualAlbumReview = reviewPageScraperFunction.apply(new ScrapeReviewPageDTO(reviewUrl: inputUrl))

        then:
            assertReflectionEquals(expectedAlbumReview, actualAlbumReview)

    }

}
