package com.ryan.amg.scraper.review.domain.mapper

import com.ryan.amg.scraper.review.domain.BandAndAlbumName
import spock.lang.Specification
import spock.lang.Unroll

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class BandAndAlbumNameMapperSpec extends Specification {

    BandAndAlbumNameMapper bandAndAlbumNameMapper = new BandAndAlbumNameMapper()

    @Unroll
    def "Invoking parseBandAndAlbumName with #inputTitle results in title=#expectedBand and album=#expectedAlbum"() {

        given:
            BandAndAlbumName expectedBandAndAlbumName = new BandAndAlbumName(band: expectedBand, album: expectedAlbum)

        when:
            BandAndAlbumName actualBandAndAlbumName = bandAndAlbumNameMapper.parseBandAndAlbumName(inputTitle)

        then:
            assertReflectionEquals(expectedBandAndAlbumName, actualBandAndAlbumName)

        where:
            inputTitle                                            | expectedBand          | expectedAlbum
            'BandName - AlbumName Review | Angry Metal Guy'       | 'BandName'            | 'AlbumName'
            'Band-Name - Album-Name Review | Angry Metal Guy'     | 'Band-Name'           | 'Album-Name'
            'Band - Name - Album - Name Review | Angry Metal Guy' | 'Band - Name - Album' | 'Name'
            'Weirdly formatted article | Angry Metal Guy'         | 'UNKNOWN'             | 'UNKNOWN'
            null                                                  | 'UNKNOWN'             | 'UNKNOWN'

    }

}
