package com.ryan.amg.scraper.review.handler.aws;

import com.ryan.amg.scraper.review.domain.AlbumReview;
import com.ryan.amg.scraper.review.dto.ScrapeReviewPageDTO;
import org.springframework.cloud.function.adapter.aws.SpringBootRequestHandler;

public class ReviewPageScraperHandler extends SpringBootRequestHandler<ScrapeReviewPageDTO, AlbumReview> {
}
