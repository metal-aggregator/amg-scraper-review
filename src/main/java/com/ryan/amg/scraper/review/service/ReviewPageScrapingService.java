package com.ryan.amg.scraper.review.service;

import com.ryan.amg.scraper.review.domain.AlbumReview;
import com.ryan.amg.scraper.review.service.amg.ReviewPageParsingService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@AllArgsConstructor
public class ReviewPageScrapingService {

    private final ReviewPageParsingService reviewPageParsingService;

    public AlbumReview retrieveAndParseAmg(String reviewUrl) throws IOException {
        return reviewPageParsingService.parseReview(reviewUrl);
    }

}
