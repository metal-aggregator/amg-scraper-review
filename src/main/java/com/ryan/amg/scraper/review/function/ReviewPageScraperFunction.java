package com.ryan.amg.scraper.review.function;

import com.ryan.amg.scraper.review.domain.AlbumReview;
import com.ryan.amg.scraper.review.dto.ScrapeReviewPageDTO;
import com.ryan.amg.scraper.review.service.ReviewPageScrapingService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.function.Function;

@Component("com.ryan.amg.scraper.review.handler.aws.ReviewPageScraperHandler")
@AllArgsConstructor
public class ReviewPageScraperFunction implements Function<ScrapeReviewPageDTO, AlbumReview> {

    private final ReviewPageScrapingService reviewPageScrapingService;

    @Override
    public AlbumReview apply(ScrapeReviewPageDTO scrapeReviewPageDTO) {
        try {
            return reviewPageScrapingService.retrieveAndParseAmg(scrapeReviewPageDTO.getReviewUrl());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
