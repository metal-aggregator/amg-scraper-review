package com.ryan.amg.scraper.review;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReviewPageScraperApplication {
    public static void main(String[] args) {
        SpringApplication.run(ReviewPageScraperApplication.class, args);
    }
}
