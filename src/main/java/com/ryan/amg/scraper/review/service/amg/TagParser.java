package com.ryan.amg.scraper.review.service.amg;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TagParser {

    private static final Logger LOG = LoggerFactory.getLogger(TagParser.class);

    private static final String TARGET_ATTRIBUTE = "rel";
    private static final String ATTRIBUTE_VALUE = "tag";

    public List<String> parseTags(Document reviewDocument) {
        Elements tagElements = reviewDocument.getElementsByAttributeValue(TARGET_ATTRIBUTE, ATTRIBUTE_VALUE);
        LOG.debug("Parsed tag elements={}", tagElements);

        return tagElements.stream().map(Element::text).collect(Collectors.toList());
    }

}
