package com.ryan.amg.scraper.review.service.amg;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class MetadataParser {

    private static final String META_TAG = "meta";
    private static final String META_PROPERTY = "property";
    private static final String META_CONTENT = "content";

    public Map<String, String> parseMetadata(Document reviewDocument) {
        Elements metaElements = reviewDocument.getElementsByTag(META_TAG);

        Map<String, String> metaElementMap = new HashMap<>();
        for (Element metaElement : metaElements) {
            String property = metaElement.attr(META_PROPERTY);
            String content = metaElement.attr(META_CONTENT);
            metaElementMap.put(property, content);
        }
        return metaElementMap;
    }

}
