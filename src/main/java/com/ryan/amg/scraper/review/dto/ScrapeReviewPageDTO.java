package com.ryan.amg.scraper.review.dto;

import lombok.Data;

@Data
public class ScrapeReviewPageDTO {
    private String reviewUrl;
}
