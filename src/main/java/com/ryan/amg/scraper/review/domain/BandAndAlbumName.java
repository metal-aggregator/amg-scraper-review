package com.ryan.amg.scraper.review.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BandAndAlbumName {
    private String band;
    private String album;
}
