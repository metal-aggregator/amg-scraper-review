package com.ryan.amg.scraper.review.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlbumReview {
    private String band;
    private String album;
    private String url;
    private List<String> tags = new ArrayList<>();
    private String imageUrl;
}
