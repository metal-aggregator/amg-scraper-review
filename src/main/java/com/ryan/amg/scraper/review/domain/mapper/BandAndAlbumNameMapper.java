package com.ryan.amg.scraper.review.domain.mapper;

import com.ryan.amg.scraper.review.domain.BandAndAlbumName;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class BandAndAlbumNameMapper {

    private static final String TITLE_REGEX = "(.*) - (.*) Review \\| Angry Metal Guy";
    private static final Pattern TITLE_PATTERN = Pattern.compile(TITLE_REGEX);

    public BandAndAlbumName parseBandAndAlbumName(@Nullable String rawTitle) {
        Matcher matcher = TITLE_PATTERN.matcher(Optional.ofNullable(rawTitle).orElse(""));

        String bandName = "UNKNOWN";
        String albumName = "UNKNOWN";

        if (matcher.find()) {
            bandName = matcher.group(1);
            albumName = matcher.group(2);
        }

        return new BandAndAlbumName(bandName, albumName);
    }

}
