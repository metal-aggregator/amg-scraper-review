package com.ryan.amg.scraper.review.domain.mapper;

import com.ryan.amg.scraper.review.domain.AlbumReview;
import com.ryan.amg.scraper.review.domain.BandAndAlbumName;
import lombok.AllArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@AllArgsConstructor
public class AlbumReviewMapper {

    private static final String TAG_TITLE = "og:title";
    private static final String TAG_URL = "og:url";
    private static final String TAG_IMAGE = "og:image:secure_url";

    private final BandAndAlbumNameMapper bandAndAlbumNameMapper;

    public AlbumReview buildAlbumReview(@NonNull Map<String, String> rawTags, List<String> tags) {
        AlbumReview albumReview = new AlbumReview();

        BandAndAlbumName bandAndAlbumName = bandAndAlbumNameMapper.parseBandAndAlbumName(rawTags.get(TAG_TITLE));
        albumReview.setBand(bandAndAlbumName.getBand());
        albumReview.setAlbum(bandAndAlbumName.getAlbum());

        albumReview.setUrl(rawTags.get(TAG_URL));
        albumReview.setImageUrl(rawTags.get(TAG_IMAGE));
        albumReview.setTags(tags);

        return albumReview;
    }

}
