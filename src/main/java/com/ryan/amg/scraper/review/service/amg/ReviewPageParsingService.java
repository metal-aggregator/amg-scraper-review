package com.ryan.amg.scraper.review.service.amg;

import com.ryan.amg.scraper.review.delegate.WebsiteDelegate;
import com.ryan.amg.scraper.review.domain.AlbumReview;
import com.ryan.amg.scraper.review.domain.mapper.AlbumReviewMapper;
import lombok.AllArgsConstructor;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Component
@AllArgsConstructor
public class ReviewPageParsingService {

    private static final Logger LOG = LoggerFactory.getLogger(ReviewPageParsingService.class);

    private final WebsiteDelegate websiteDelegate;
    private final MetadataParser metadataParser;
    private final TagParser tagParser;
    private final AlbumReviewMapper albumReviewMapper;

    public AlbumReview parseReview(String reviewUrl) throws IOException {
        Document reviewDocument = websiteDelegate.retrieveWebsite(reviewUrl);
        LOG.debug("ReviewDocument={}", reviewDocument.toString());

        Map<String, String> reviewMetadata = metadataParser.parseMetadata(reviewDocument);
        List<String> reviewTags = tagParser.parseTags(reviewDocument);
        return albumReviewMapper.buildAlbumReview(reviewMetadata, reviewTags);
    }

}
