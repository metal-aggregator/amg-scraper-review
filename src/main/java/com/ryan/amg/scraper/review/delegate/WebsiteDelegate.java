package com.ryan.amg.scraper.review.delegate;

import com.ryan.amg.scraper.review.delegate.remote.DocumentFetcher;
import lombok.AllArgsConstructor;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@AllArgsConstructor
public class WebsiteDelegate {

    private final DocumentFetcher documentFetcher;

    public Document retrieveWebsite(String url) throws IOException {
        return documentFetcher.fetchDocument(url);
    }

}
